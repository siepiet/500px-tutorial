//
//  AppDelegate.h
//  500pxv2
//
//  Created by mateusz on 02.11.2015.
//  Copyright © 2015 mateusz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

