//
//  LoginPageViewController.h
//  500pxv2
//
//  Created by mateusz on 02.11.2015.
//  Copyright © 2015 mateusz. All rights reserved.
//

#import "ViewController.h"

@interface LoginPageViewController : ViewController
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;

@end
